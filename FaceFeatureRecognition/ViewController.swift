//
//  ViewController.swift
//  FaceFeatureRecognition
//
//  Created by Forrest Vander Borgh on 10/20/17.
//  Copyright © 2017 Forrest Vander Borgh. All rights reserved.
//

import UIKit
import Vision
import AVFoundation

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    private let session = AVCaptureSession()
    private var requests = [VNRequest]()
    
    let shapeLayer = CAShapeLayer()
    
    let faceDetection = VNDetectFaceRectanglesRequest()
    let faceLandmarks = VNDetectFaceLandmarksRequest()
    let faceLandmarksDetectionRequest = VNSequenceRequestHandler()
    let faceDetectionRequest = VNSequenceRequestHandler()
    
    @IBOutlet weak var cameraPreviewView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        startCamera()

    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        cameraPreviewLayer?.frame = view.frame
//        shapeLayer.frame = view.frame
//    }
//
    func startCamera() {
        let cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        let backCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        let input = try? AVCaptureDeviceInput(device: backCamera!)
        let output = AVCaptureVideoDataOutput()
        
        output.videoSettings = [kCVPixelBufferPixelFormatTypeKey as AnyHashable as! String: NSNumber(value: kCVPixelFormatType_32BGRA)]
        output.alwaysDiscardsLateVideoFrames = true
        output.setSampleBufferDelegate(
            self,
            queue: DispatchQueue(
                label: "buffer queue",
                qos: .userInteractive,
                attributes: .concurrent,
                autoreleaseFrequency: .inherit,
                target: nil
            )
        )
        
        
        session.addOutput(output)
        session.addInput(input!)
        
        cameraPreviewLayer.frame = cameraPreviewView.bounds
        cameraPreviewView.layer.addSublayer(cameraPreviewLayer)
        
        shapeLayer.frame = cameraPreviewView.frame//view.frame
        
        shapeLayer.setAffineTransform(CGAffineTransform(scaleX: -1, y: -1))
        cameraPreviewView.layer.addSublayer(shapeLayer)
        
        session.startRunning()
    }
    
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            print("unable to retrieve image buffer")
            return
        }
        connection.videoOrientation = AVCaptureVideoOrientation.portrait
        

        let image = CIImage(cvPixelBuffer: pixelBuffer)
        
        self.handleFeatures(on: image)
    }
    
    func handleFeatures(on image: CIImage) {
        try? faceDetectionRequest.perform([faceDetection], on: image)
        if let results = faceDetection.results as? [VNFaceObservation] {
            if !results.isEmpty {
                faceLandmarks.inputFaceObservations = results
                detectLandmarks(on: image)
            }
        }
    }
    
    func detectLandmarks(on image: CIImage) {
        try? faceLandmarksDetectionRequest.perform([faceLandmarks], on: image)

        if let landmarksResults = faceLandmarks.results as? [VNFaceObservation] {
            self.shapeLayer.sublayers?.removeAll()
            for observation in landmarksResults {
                DispatchQueue.main.async {
                    if let boundingBox = self.faceLandmarks.inputFaceObservations?.first?.boundingBox {
                        let faceBoundingBox = boundingBox.scaled(to: self.view.bounds.size)
                        
                        //different types of landmarks
                        let faceContour = observation.landmarks?.faceContour
                        self.convertPointsForFace(faceContour, faceBoundingBox)
                        
                        let leftEye = observation.landmarks?.leftEye
                        self.convertPointsForFace(leftEye, faceBoundingBox)
                        
                        let rightEye = observation.landmarks?.rightEye
                        self.convertPointsForFace(rightEye, faceBoundingBox)
// I dont think Pupils work in the current build of Vision SDK -Forrest Oct 22, 2017
//                        let leftPupil = observation.landmarks?.leftPupil
//                        self.convertPointsForFace(leftPupil, faceBoundingBox)
//
//                        let rightPupil = observation.landmarks?.rightPupil
//                        self.convertPointsForFace(rightPupil, faceBoundingBox)
                        
                        let nose = observation.landmarks?.nose
                        self.convertPointsForFace(nose, faceBoundingBox)
                        
                        let lips = observation.landmarks?.innerLips
                        self.convertPointsForFace(lips, faceBoundingBox)
                        
                        let leftEyebrow = observation.landmarks?.leftEyebrow
                        self.convertPointsForFace(leftEyebrow, faceBoundingBox)
                        
                        let rightEyebrow = observation.landmarks?.rightEyebrow
                        self.convertPointsForFace(rightEyebrow, faceBoundingBox)
                        
                        let noseCrest = observation.landmarks?.noseCrest
                        self.convertPointsForFace(noseCrest, faceBoundingBox)
                        
                        let outerLips = observation.landmarks?.outerLips
                        self.convertPointsForFace(outerLips, faceBoundingBox)
                    }
                }
            }
        }
    }
    
    
    func convertPointsForFace(_ landmark: VNFaceLandmarkRegion2D?, _ boundingBox: CGRect) {
        if let points = landmark?.normalizedPoints {
            let convertedPoints = points //convert(points, with: count)
            
            let faceLandmarkPoints = convertedPoints.map { (point: CGPoint) -> (x: CGFloat, y: CGFloat) in
                let pointX = point.x * boundingBox.width + boundingBox.origin.x
                let pointY = point.y * boundingBox.height + boundingBox.origin.y
                
                return (x: pointX, y: pointY)
            }
            
            DispatchQueue.main.async {
                self.draw(points: faceLandmarkPoints)
            }
        }
    }
    
    func draw(points: [(x: CGFloat, y: CGFloat)]) {
        let newLayer = CAShapeLayer()
        newLayer.strokeColor = UIColor.red.cgColor
        newLayer.lineWidth = 1.0
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: points[0].x, y: points[0].y))
        for i in 0...points.count - 1 {
            let point = CGPoint(x: points[i].x, y: points[i].y)
            path.addLine(to: point)
            path.move(to: point)
        }
        path.addLine(to: CGPoint(x: points[0].x, y: points[0].y))
        newLayer.path = path.cgPath
        
        shapeLayer.addSublayer(newLayer)
    }

}

extension CGRect {
    func scaled(to size: CGSize) -> CGRect {
        return CGRect(
            x: self.origin.x * size.width,
            y: self.origin.y * size.height,
            width: self.size.width * size.width,
            height: self.size.height * size.height
        )
    }
}

